#!/usr/bin/env python
# -*- coding: utf-8 -*-

import glob
from setuptools import setup

readme = open('README.md').read()

setup(
    name='django-diu',
    version='0.0.0',
    description='Django Data Import Utility',
    long_description=readme,
    author='Hervé Ménager',
    author_email='hmenager@pasteur.fr',
    packages=['django_diu'],
    install_requires=[
        'django',
        'requests_cache',
        'click',
        'pandas',
        'mysql-connector'
    ],
    license="BSD"
)