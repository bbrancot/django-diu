from io import StringIO

from django.db import models
from django_diu import import_command

from tests.models import AModel

class AImportTask(import_command.ListImportTask):

    description = "A very basic 'import from list' test task"

    DATA = [
        'A','B','C','D'
    ]

    option = "a"

    target_classes = [AModel]

    main_class = AModel

    depends_on = []

    def migrate_row(self, row):
        a = AModel()
        a.a_field = row[0]
        a.save()
        return a


class Command(import_command.ImportCommand):

    help = "Test command to test django-diu's ImportCommand"

    # list all the import tasks that should be available via the command
    task_classes = [AImportTask]