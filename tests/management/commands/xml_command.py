import os

from django.db import models
from django_diu import import_command

from tests.models import AModel

class XMLImportTask(import_command.XMLImportTask):

    description = "A very basic 'import from XML' test task"

    option = "b"

    target_classes = [AModel]

    main_class = AModel

    depends_on = []

    xpathSelector = 'a'

    def __init__(self, command, **kwargs):
        super().__init__(command, **kwargs)
        self.xmlFile = command.xmlFile

    def migrate_row(self, row):
        a = AModel()
        a.a_field = row.get("value")[0]
        a.save()
        return a


class Command(import_command.ImportCommand):

    help = "Test command to test django-diu's ImportCommand"

    xmlFile = os.path.join(os.path.dirname(__file__),'xml_command_file.xml')

    # list all the import tasks that should be available via the command
    task_classes = [XMLImportTask]